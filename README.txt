Health Check URL

 Health Check URL module is built to provide an health endpoint for
 all type of load balancers, this endpoint URI will say that this
 Drupal site is up.
 Health Check URL provides five different formats of response with the
 combination of string & timestamp which is configurable and
 the endpoint URL can be configured and its not restricted to one endpoint.


 Features
 
  1. Endpoint configurable
  2. String configurable
  3. Format Configurable

